namespace MoovMoney.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using MoovMoney.Models;
    using System;
    using Microsoft.AspNet.Identity.Owin;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web;

    internal sealed class Configuration : DbMigrationsConfiguration<MoovMoney.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MoovMoney.Models.ApplicationDbContext context)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            string[] roleNames = { RoleConstant.ADMIN, RoleConstant.AUDITEUR, RoleConstant.CAISSIER };
            string name = "admin@admin.com";
            string password = "admin123";
            string phone = "96271395";

            //Create roles
            foreach (string roleName in roleNames)
            {
                if (roleManager.FindByName(roleName) == null)
                {
                    var role = new IdentityRole(roleName);
                    roleManager.Create(role);
                }
            }

            //Create super admin and add to all roles
            if (userManager.FindByName(name) == null)
            {
                var user = new ApplicationUser() { UserName = name, Email = name, PhoneNumber = phone,IsValidated=true };
                userManager.Create(user, password);
                userManager.AddToRoles(user.Id, roleNames);
            }

            

            
            

            
            
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
