﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MoovMoney.Models;
using MoovMoney.ViewModels;
using OfficeOpenXml;
using System.IO;
using Syroot.Windows.IO;
using log4net;

namespace MoovMoney.Controllers
{
    public class TransactionListController : BaseController
    {
        public static readonly ILog logger = LogManager.GetLogger("UserTransactions");
        // GET: TransactionList
        public async Task<ActionResult> Index(string filter)
        {
            try
            {
                var trans = await GetUserTransactions();
                switch (filter)
                {
                    case "WEEK":
                        DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek), // prev sunday 00:00
                                 endDate = startDate.AddDays(7);
                        trans = trans.Where(t => (t.timestamp.Date >= startDate && t.timestamp.Date < endDate)).ToList();
                        ViewBag.FilterTitle = "Cette semaine";
                        break;
                    case "MONTH":
                        trans = trans.Where(t => (t.timestamp.Date.Month == DateTime.Today.Month && t.timestamp.Date.Year == DateTime.Today.Year)).ToList();
                        ViewBag.FilterTitle = "Ce mois";
                        break;
                    default:
                        trans = trans.Where(t => t.timestamp.Date == DateTime.Now.Date).ToList();
                        ViewBag.FilterTitle = "Aujourd'hui";
                        break;
                }
                var vm = new TransactionListVM { Transactions = trans };
                return View(vm);
            }
            catch (Exception ex)
            {
                logger.Error(CurrentUser.UserName, ex);
                throw;
            }
            
        }

        [HttpPost]
        public async Task<ActionResult> Index(TransactionListVM vm)
        {
            try
            {
                var trans = await GetUserTransactions();
                if (!string.IsNullOrWhiteSpace(vm.PhoneNumber))
                    trans = trans.Where(t => t.msisdn.Contains(vm.PhoneNumber.Trim())).ToList();
                vm.Transactions = trans;
                return View(vm);
            }
            catch (Exception ex)
            {
                logger.Error(CurrentUser.UserName, ex);
                throw;
            }
            
        }

        public ActionResult ExportToExcel()
        {
            try
            {
                var DirPath = KnownFolders.PublicDownloads.Path;
                String fileName = Path.Combine(DirPath, "transactions.xlsx");
                var trans = DbManager.Transactions.ToList();
                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets.Add("Transactions");
                    worksheet.Row(1).Height = 20;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Cells[1, 1].Value = "N°";
                    worksheet.Cells[1, 2].Value = "NOM";
                    worksheet.Cells[1, 3].Value = "PRENOMS";
                    worksheet.Cells[1, 4].Value = "NUMERO";
                    worksheet.Cells[1, 5].Value = "MONTANT";
                    worksheet.Cells[1, 6].Value = "DATE";
                    for (int i = 0; i < trans.Count; i++)
                    {
                        Transaction t = trans[i];
                        worksheet.Cells[i + 2, 1].Value = i + 1;
                        worksheet.Cells[i + 2, 2].Value = t.lastname;
                        worksheet.Cells[i + 2, 3].Value = t.firstname;
                        worksheet.Cells[i + 2, 4].Value = t.msisdn;
                        worksheet.Cells[i + 2, 5].Value = t.transamount;
                        worksheet.Cells[i + 2, 6].Value = t.timestamp.ToLongDateString();

                        worksheet.Column(i + 1).AutoFit();
                    }

                    //Create excel file on physical disk    
                    FileInfo info = new FileInfo(fileName);
                    pck.SaveAs(info);
                }
                return File(fileName, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Transactions.xlsx");
            }
            catch (Exception ex)
            {
                logger.Error(CurrentUser.UserName, ex);
                throw;
            }
            
        }

        private async Task<List<Transaction>> GetUserTransactions()
        {
            var id = CurrentUser.Id;
            return await DbManager.Transactions.Where(t => t.User.Id == id)
                                    .OrderByDescending(t => t.timestamp).ToListAsync();
        }

        
        
       
    }
}
