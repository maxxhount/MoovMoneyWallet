﻿using MoovMoney.Helpers;
using MoovMoney.Models;
using MoovMoney.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using log4net;
using MoovMoney.Service;


namespace MoovMoney.Controllers
{
    [Authorize()]
    public class HomeController : BaseController
    {
        public static readonly ILog logger = LogManager.GetLogger("Home");
         
        public async Task<ActionResult> Index()
        {
            int count = RequestCountHelper.Instance.Counter;
            await new MMWClient().GetMobileStatusAsync("22994969696",count);
            RequestCountHelper.Instance.Counter = count++;
            if (!User.IsInRole(RoleConstant.CAISSIER))
                return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
            try
            {
                
                long amount = GetUserTransactionsTotalAmount(CurrentUser.Id);
                Common.TotalCalculator.TotalTransactionsOfDay = PriceFormater.Format(amount);               
            }
            catch (Exception ex)
            {
                logger.Error(CurrentUser.UserName, ex);
                throw;
            }
            
            return View();
        }

        

        private long GetUserTransactionsTotalAmount(string userId)
        {
             return DbManager.Transactions.Where(t => t.User.Id == userId).ToList()
                .Where(tr => tr.timestamp.Date == DateTime.Now.Date)
                .Sum(tr => tr.transamount);
        }

        [Authorize(Roles = "CAISSIER")]
        [HttpPost]
        public async Task<ActionResult> Index(AddOperationVM vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Transaction t = new Transaction
                    {
                        firstname = vm.Prenoms,
                        lastname = vm.Nom,
                        msisdn = "229" + vm.Numero,
                        secondname = vm.Prenoms,
                        timestamp = DateTime.Now,
                        transamount = long.Parse(vm.Montant),
                        banktransactionreferenceid = Guid.NewGuid().ToString("N"),
                        User = CurrentUser
                    };
                    if (IsMoovPhoneNumber(vm.Numero))
                    {
                        var moov = DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MOOV);
                        t.accountnumber = moov.AccountNumber;
                        t.bankbalancebefore = moov.CurrentBalance;
                        t.bankbalanceafter = CalculateNewBalance(long.Parse(vm.Montant), moov);
                        t.IsMoov = true;
                        SendMoneyToWallet(t);
                        await UpdateNetworkAccountBalanceAsync(moov, t);
                    }
                    else if (IsMTNPhoneNumber(vm.Numero))
                    {
                        var mtn = DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MTN);
                        t.accountnumber = mtn.AccountNumber;
                        t.bankbalancebefore = mtn.CurrentBalance;
                        t.bankbalanceafter = CalculateNewBalance(long.Parse(vm.Montant), mtn);
                        t.IsMtn = true;
                        SendMoneyToWallet(t);
                        await UpdateNetworkAccountBalanceAsync(mtn, t);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Le numero entré n'est ni un numero MOOV, ni un numero MTN");
                        return View(vm);
                    }
                    await SaveOperationAsync(t);
                    TempData["TransfertDone"] = true;
                    return RedirectToAction("Index");
                }
                return View(vm);
            }
            catch (Exception ex)
            {
                logger.Error(CurrentUser.UserName, ex);
                throw;
            }           
        }
       

        private bool IsMoovPhoneNumber(string phoneNumber)
        {
            var moovprefixes = DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MOOV).Prefixes.Select(p => p.Prefix).ToList();
            var clientNumberPrefix = phoneNumber.Substring(0, 2);
            return moovprefixes.Contains(clientNumberPrefix);
        }

        private bool IsMTNPhoneNumber(string phoneNumber)
        {
            var moovprefixes = DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MTN).Prefixes.Select(p => p.Prefix).ToList();
            var clientNumberPrefix = phoneNumber.Substring(0, 2);
            return moovprefixes.Contains(clientNumberPrefix);
        }


        private Task<int> UpdateNetworkAccountBalanceAsync(Network account, Transaction t)
        {
            account.CurrentBalance = t.bankbalanceafter;
            DbManager.Networks.AddOrUpdate(a => a.AccountNumber, account);
            return DbManager.SaveChangesAsync();
        }

        private Task<int> SaveOperationAsync(Transaction t)
        {        
            DbManager.Transactions.Add(t);
            return DbManager.SaveChangesAsync();
        }

        private long CalculateNewBalance(long amount, Network network)
        {
            long currentBalance = network.CurrentBalance;
            return currentBalance + amount;
        }

        private void SendMoneyToWallet(Transaction tr)
        {
            //TODO: Implement Wallet API
        }
    }
}