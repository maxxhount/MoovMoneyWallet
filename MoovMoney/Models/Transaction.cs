﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public class Transaction
    {
        public Transaction()
        {
            this.Id = Guid.NewGuid().ToString("N");
        }
        public string Id { get; set; }
        public string banktransactionreferenceid { get; set; }
        public string msisdn { get; set; }
        public string firstname { get; set; }
        public string secondname { get; set; }
        public string lastname { get; set; }
        public DateTime timestamp { get; set; }
        public string accountnumber { get; set; }
        public long transamount { get; set; }
        public long bankbalancebefore { get; set; }
        public long bankbalanceafter { get; set; }
        public bool? IsMoov { get; set; }
        public bool? IsMtn { get; set; }
        //public virtual Operation Operation { get; set; }

        public virtual ApplicationUser User { get; set; }

    }
}