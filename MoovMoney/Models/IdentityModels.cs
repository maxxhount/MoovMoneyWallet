﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using MoovMoney.Migrations;

namespace MoovMoney.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser():base()
        {
            Transactions = new List<Transaction>();
        }
            
        public string Nom { get; set; }
        public string Prenoms { get; set; }
        public string TempPassword { get; set; }
        public string Matricule { get; set; }
        public string AdresseIP { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? DateAjout { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        public virtual Agence UserAgence { get; set; }
        public bool IsValidated { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Network> Networks { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Agence> Agences { get; set; }
        public DbSet<RequestCounter> RequestCounter { get; set; }


        public ApplicationDbContext()
            : base("MoovMoneyDbContext", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(
                new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>()
                );
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}