﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public static class RoleConstant
    {
        public static string ADMIN { get { return "ADMIN"; } }
        public static string CAISSIER { get { return "CAISSIER"; } }
        public static string AUDITEUR { get { return "AUDITEUR"; } }
    }
}