﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public class BarChartDataSet
    {       
        public string MoovData { get; set; }
        public string MtnData { get; set; }
    }
}