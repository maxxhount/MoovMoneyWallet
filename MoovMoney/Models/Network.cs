﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public class Network
    {
        public Network()
        {
            Prefixes = new List<NetworkPrefix>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public long CurrentBalance { get; set; }
        public string AccountNumber { get; set; }
        public virtual ICollection<NetworkPrefix> Prefixes { get; set; }
    }
}