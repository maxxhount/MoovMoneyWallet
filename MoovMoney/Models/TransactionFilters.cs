﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public static class TransactionFilters
    {
        public static string TODAY { get { return "TODAY"; } }
        public static string THIS_WEEK { get { return "WEEK"; } }
        public static string THIS_MONTH { get { return "MONTH"; } }
    }
}