﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public class NetworkPrefix
    {
        public int Id { get; set; }
        public string Prefix { get; set; }

        public virtual Network Network { get; set; }
    }
}