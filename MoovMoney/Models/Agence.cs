﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public class Agence
    {
        public int Id { get; set; }
        public string Nom { get; set; }
    }
}