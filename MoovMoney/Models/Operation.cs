﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Models
{
    public class Operation
    {
        public Operation()
        {
            this.Id = Guid.NewGuid().ToString("N");
            this.Transactions = new List<Transaction>();
        }
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual List<Transaction> Transactions { get; set; }
    }
}