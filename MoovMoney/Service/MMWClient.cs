﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoovMoney.Helpers;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using MoovMoney.Models;

namespace MoovMoney.Service
{
    public class MMWClient
    {
        private string baseAddressTest = "https://10.18.28.163:1443",endpointTest = "/MMWServiceTest?wsdl";
        private string baseAddressLive = "https://10.18.28.163:443",endpointLive = "/MMWService?wsdl";

        public MMWClient()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
        }


        public async Task<Response.Response> GetMobileStatusAsync(string msisdn, int count)
        {
            using(HttpClient client=new HttpClient())
            {                
                //ServicePointManager.CertificatePolicy = new MyPolicy();
               // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
               
                string requestbody = new RequestBodyBuilder(count).GetStatusRequestBody(msisdn);
                StringContent content = new StringContent(requestbody);
                client.BaseAddress = new Uri(baseAddressTest);
                var result=await client.PostAsync(endpointTest, content);
                var resultString=await result.Content.ReadAsStringAsync();
                return XmlDeserializer.XmlDeserialize<Response.Response>(resultString);
            }
        }

        public async Task<string> BankToWalletAsync(Transaction t,int count)
        {
            using (HttpClient client = new HttpClient())
            {
                string requestbody = new RequestBodyBuilder(count).GetBankToWalletRequestBody(t);
                StringContent content = new StringContent(requestbody);
                client.BaseAddress = new Uri(baseAddressTest);
                var result = await client.PostAsync(endpointTest, content);
                var resultString = await result.Content.ReadAsStringAsync();
                return resultString;
            }
        }
    }
}