﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoovMoney.Helpers;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using MoovMoney.Models;
using RestSharp;
using MoovMoney.Service.Response;

namespace MoovMoney.Service
{
    public class MMRestSharpClient
    {
        private string baseAddressTest = "https://10.18.28.163:1443/MMServiceTEST?wsdl", endpointTest = "/MMServiceTEST?wsdl";
        private string baseAddressLive = "https://10.18.28.163:443",endpointLive = "/MMService?wsdl";

        public MMRestSharpClient()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
        }


        public Service.Response.Response GetMobileStatusAsync(string msisdn)
        {
                
                //ServicePointManager.CertificatePolicy = new MyPolicy();
               // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                string requestbody = new RequestBodyBuilder(0).GetStatusRequestBody(msisdn);
                StringContent content = new StringContent(requestbody);
                var request = new RestRequest(Method.POST) 
                { 
                    RequestFormat = DataFormat.Xml, 
                    XmlSerializer = new RestSharp.Serializers.DotNetXmlSerializer()
                };
                //request.AddParameter("text/xml", body, ParameterType.RequestBody);
                request.AddBody(requestbody);
                var resp = ExecuteAsync<Service.Response.Response>(request);
                return resp;
            
        }

        public async Task<string> BankToWalletAsync(Transaction t)
        {
            using (HttpClient client = new HttpClient())
            {
                string requestbody = new RequestBodyBuilder(0).GetBankToWalletRequestBody(t);
                StringContent content = new StringContent(requestbody);
                client.BaseAddress = new Uri(baseAddressTest);
                var result = await client.PostAsync(endpointTest, content);
                var resultString = await result.Content.ReadAsStringAsync();
                return resultString;
            }
        }

        public T ExecuteAsync<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(baseAddressTest, UriKind.Absolute);
            var response = new T();
            client.ExecuteAsync<T>(request, r => { response = r.Data; });
            return response;
        }
    }
}