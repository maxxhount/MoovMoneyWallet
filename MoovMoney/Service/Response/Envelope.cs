﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MoovMoney.Service.Response
{
    
        [XmlRoot(ElementName = "response")]
        public class Response
        {
            [XmlElement(ElementName = "accounttype")]
            public string Accounttype { get; set; }
            [XmlElement(ElementName = "allowedtransfer")]
            public string Allowedtransfer { get; set; }
            [XmlElement(ElementName = "city")]
            public string City { get; set; }
            [XmlElement(ElementName = "dateofbirth")]
            public string Dateofbirth { get; set; }
            [XmlElement(ElementName = "firstname")]
            public string Firstname { get; set; }
            [XmlElement(ElementName = "lastname")]
            public string Lastname { get; set; }
            [XmlElement(ElementName = "message")]
            public string Message { get; set; }
            [XmlElement(ElementName = "msisdn")]
            public string Msisdn { get; set; }
            [XmlElement(ElementName = "region")]
            public string Region { get; set; }
            [XmlElement(ElementName = "secondname")]
            public string Secondname { get; set; }
            [XmlElement(ElementName = "status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "street")]
            public string Street { get; set; }
            [XmlElement(ElementName = "subscriberstatus")]
            public string Subscriberstatus { get; set; }
        }

        [XmlRoot(ElementName = "getMobileAccountStatusResponse", Namespace = "http://mmwservice/")]
        public class GetMobileAccountStatusResponse
        {
            [XmlElement(ElementName = "response")]
            public Response Response { get; set; }
            [XmlAttribute(AttributeName = "ns2", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Ns2 { get; set; }
        }

        [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public class Body
        {
            [XmlElement(ElementName = "getMobileAccountStatusResponse", Namespace = "http://mmwservice/")]
            public GetMobileAccountStatusResponse GetMobileAccountStatusResponse { get; set; }
        }

        [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public class Envelope
        {
            [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
            public Body Body { get; set; }
            [XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Soap { get; set; }
        }
    
}