﻿using MoovMoney.Areas.Admin.ViewModels;
using MoovMoney.Controllers;
using MoovMoney.Helpers;
using MoovMoney.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MoovMoney.Areas.Admin.Controllers
{
    [Authorize(Roles="ADMIN, AUDITEUR")]
    public class DashboardController : BaseController
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            var vm = new DashbordViewModel();
            vm.MoovBalanceOfDay = GetMoovWeeklyBalance();
            vm.MtnBalanceOfDay = GetMtnWeeklyBalance();
            vm.CaissierCount = DbManager.Users.Count();
            vm.OperationOfWeekCount = GetTotalOperationOfWeekCount();
            vm.TotalMerchentThisWeek = GetTotalMerchentThisWeek();
            vm.TotalMerchent = GetTotalMerchent();
            vm.TransactionOfYearJson = GetYearTransactionJsonData();
            vm.RecentTransactions = DbManager.Transactions.ToList().OrderByDescending(t=>t.timestamp.Date).Take(10).ToList();
            vm.AgenceTransactions = GetAgenceTransactions();
            return View(vm);
        }

        private List<AgenceVM> GetAgenceTransactions()
        {
            var list = new List<AgenceVM>();
            foreach(var agence in DbManager.Agences.ToList())
            {
                var agenceTotalAmount = DbManager.Transactions
                    .Where(t => t.User.UserAgence != null && t.User.UserAgence.Id == agence.Id).ToList().Sum(t => t.transamount);
                var item = new AgenceVM { AgenceName = agence.Nom, TotalAmount = PriceFormater.Format(agenceTotalAmount) };
                list.Add(item);
            }
            return list.OrderByDescending(ag=>ag.TotalAmount).ToList();
        }

        private long GetTotalMerchentThisWeek()
        {
            DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek), // prev sunday 00:00
                             endDate = startDate.AddDays(7);
            var result = DbManager.Transactions.ToList().Where(t => (t.timestamp.Date >= startDate && t.timestamp.Date < endDate))
                .Select(t => t.msisdn).Distinct().Count();
            return result;
        }

        private long GetTotalMerchent()
        {
            var result = DbManager.Transactions
                .Select(t=>t.msisdn).Distinct().Count();
            return result;
        }

        

        private string GetMoovWeeklyBalance()
        {
            DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek), // prev sunday 00:00
                             endDate = startDate.AddDays(7);
            var transOfWeek = DbManager.Transactions.ToList().Where(t => (t.IsMoov == true && t.timestamp.Date == DateTime.Today.Date)).ToList();
            return PriceFormater.Format(transOfWeek.Sum(t => t.transamount));
        }
        private string GetMtnWeeklyBalance()
        {
            DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek), // prev sunday 00:00
                             endDate = startDate.AddDays(7);
            var transOfWeek = DbManager.Transactions.ToList().Where(t => (t.IsMtn == true && t.timestamp.Date == DateTime.Today.Date)).ToList();
            return PriceFormater.Format(transOfWeek.Sum(t => t.transamount));
        }

        private int GetTotalOperationOfWeekCount()
        {
            DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek), // prev sunday 00:00
                            endDate = startDate.AddDays(7);
            var count = DbManager.Transactions.ToList().Where(t => (t.timestamp.Date >= startDate && t.timestamp.Date < endDate)).Count();
            return count;
        }

        private BarChartDataSet GetYearTransactionJsonData()
        {
            var moovbars = new List<long>();
            var mtnbars = new List<long>();
            var bars = new BarChartDataSet();
            
            var trans = DbManager.Transactions.ToList().Where(t => t.timestamp.Year == DateTime.Now.Year);
            for(int month=1;month<=12;month++)
            {
                var moovMonthTrans = trans.Where(t => t.timestamp.Month == month && t.IsMoov == true).ToList();
                if (moovMonthTrans != null && moovMonthTrans.Count > 0)
                    moovbars.Add(moovMonthTrans.Sum(t => t.transamount));
                else
                    moovbars.Add(0);
                var mtnMontTrans = trans.Where(t => t.timestamp.Month == month && t.IsMtn == true).ToList();
                if (mtnMontTrans != null && mtnMontTrans.Count > 0)
                    mtnbars.Add(mtnMontTrans.Sum(t => t.transamount));
                else
                    mtnbars.Add(0);
            }
            bars.MtnData = JsonConvert.SerializeObject(mtnbars);
            bars.MoovData = JsonConvert.SerializeObject(moovbars);
            return bars;
        }
    }
}