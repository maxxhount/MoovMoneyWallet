﻿using MoovMoney.Controllers;
using MoovMoney.Helpers;
using MoovMoney.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Syroot.Windows.IO;
using MoovMoney.Areas.Admin.ViewModels;
using System.Globalization;

namespace MoovMoney.Areas.Admin.Controllers
{
    [Authorize(Roles = "ADMIN, AUDITEUR")]
    public class ReportingController : BaseController
    {
        // GET: Admin/Reporting
        public ActionResult Index()
        {
            var vm = new ReportingViewModel();
            vm.Agences=GetAgenceSelectList();
            return View(vm);
        }

        private List<SelectListItem> GetAgenceSelectList()
        {
            return DbManager.Agences.Select(a => new SelectListItem { Text = a.Nom, Value = a.Id.ToString() }).ToList();
        }

        [HttpPost]
        public ActionResult Index(ReportingViewModel vm)
        {
            DateTime startDate = DateTime.ParseExact(vm.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime endDate = DateTime.ParseExact(vm.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var trans = DbManager.Transactions.ToList().Where(t => t.timestamp.Date >= startDate && t.timestamp.Date <= endDate).ToList();            
            if (vm.IsMoov==true && vm.IsMtn==false)
                trans = trans.Where(t => t.IsMoov==true).ToList();
            if (vm.IsMtn==true && vm.IsMoov==false)
                trans = trans.Where(t => t.IsMtn == true).ToList();
            if (!string.IsNullOrEmpty(vm.PhoneNumber))
                trans = trans.Where(t => t.msisdn.Contains(vm.PhoneNumber.Trim())).ToList();
            if(vm.Agence!=null)
            {
                int agenceId = int.Parse(vm.Agence);
                trans = trans.Where(t => t.User.UserAgence != null && t.User.UserAgence.Id == agenceId).ToList();
            }

            vm.Agences = GetAgenceSelectList();
            vm.Transactions = trans;
            vm.IsPost = true;
            return View(vm);
        }

        public async Task<ActionResult> GetDailyReport()
        {
            try
            {
                //MOOV WORKSHEET DATA
                string moovWorksheetName = "MOOV MONEY TRANSACTIONS";
                var moov = await DbManager.Networks.FirstOrDefaultAsync(n => n.Name == Common.NetworkName.MOOV);
                var moovDailyTransactions = DbManager.Transactions.ToList().Where(t => t.timestamp.Date == DateTime.Today.Date && t.IsMoov == true).ToList();

                //MTN WORKSHEET DATA
                string mtnWorksheetName = "MTN MONEY TRANSACTIONS";
                var mtn = await DbManager.Networks.FirstOrDefaultAsync(n => n.Name == Common.NetworkName.MTN);
                var mtnDailyTransactions = DbManager.Transactions.ToList().Where(t => t.timestamp.Date == DateTime.Today.Date && t.IsMtn == true).ToList();

                //EXCEL FILE DATA
                string nameToDisplay = "Transactions_" + DateTime.Now + ".xlsx";
                var DirPath = KnownFolders.PublicDownloads.Path;
                String fileName = Path.Combine(DirPath, "transactions.xlsx");

                using (ExcelPackage pck = new ExcelPackage())
                {
                    GenerateExcelWorksheet(moovWorksheetName, moov, GetUsersWhoMadeMoovTransactions(), moovDailyTransactions, pck);
                    GenerateExcelWorksheet(mtnWorksheetName, mtn, GetUsersWhoMadeMtnTransactions(), mtnDailyTransactions, pck);

                    //Create excel file on physical disk    
                    FileInfo info = new FileInfo(fileName);
                    pck.SaveAs(info);
                }

                return File(fileName, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", nameToDisplay);
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }



        private void GenerateExcelWorksheet(string sheetName,Network network,List<ApplicationUser> usrs,List<Transaction> networkDailyTrans,ExcelPackage pck)
        {                        
                
                var users = usrs;
                ExcelWorksheet worksheet = pck.Workbook.Worksheets.Add(sheetName);
                worksheet.Row(1).Height = 20;
                worksheet.Row(1).Style.Font.Bold = true;
                worksheet.Cells[1, 1].Value = "NAME";
                worksheet.Cells[1, 2].Value = "ACCT NR";
                worksheet.Cells[1, 3].Value = "AMOUNT";
                worksheet.Cells[1, 4].Value = "";
                worksheet.Cells[1, 5].Value = "PIN";
                worksheet.Cells[1, 6].Value = "TRAN TYPE";
                worksheet.Cells[1, 7].Value = "ACC OFF CODE";



            if(users!=null && users.Count>0)
            {
                int count = users.Count;
                for (int i = 0; i < count; i++)
                {
                    var user = users.ElementAt(i);
                    var userTransTotalAmount = networkDailyTrans.Where(t => t.User.Id == user.Id).ToList().Sum(t => t.transamount);

                    worksheet.Cells[i + 2, 1].Value = user.UserName;
                    worksheet.Cells[i + 2, 2].Value = user.AccountNumber;
                    worksheet.Cells[i + 2, 3].Value = userTransTotalAmount;
                    worksheet.Cells[i + 2, 4].Value = "";
                    worksheet.Cells[i + 2, 5].Value = "";
                    worksheet.Cells[i + 2, 6].Value = "D";
                    worksheet.Cells[i + 2, 7].Value = "";
                    worksheet.Column(i + 1).AutoFit();
                    if (userTransTotalAmount == 0)
                        worksheet.DeleteRow(i + 2);
                }
                worksheet.Cells[count + 2, 1].Value = network.Name;
                worksheet.Cells[count + 2, 2].Value = network.AccountNumber;
                worksheet.Cells[count + 2, 3].Value = networkDailyTrans.Sum(t => t.transamount);
                worksheet.Cells[count + 2, 4].Value = "";
                worksheet.Cells[count + 2, 5].Value = "";
                worksheet.Cells[count + 2, 6].Value = "C";
                worksheet.Cells[count + 2, 7].Value = "";
                worksheet.Row(count + 2).Style.Font.Bold = true;
            }           
        }
         private List<ApplicationUser> GetUsersWhoMadeMoovTransactions()
        {
            var list = new List<ApplicationUser>();
             foreach(var user in DbManager.Users.ToList())
             {
                 var userTrans = user.Transactions.Where(t => t.IsMoov == true).ToList().Where(t => t.timestamp.Date == DateTime.Today.Date).ToList();
                 if (userTrans!=null && userTrans.Count() > 0)
                     list.Add(user);
             }
            return list;
        }
         private List<ApplicationUser> GetUsersWhoMadeMtnTransactions()
         {
             var list = new List<ApplicationUser>();
             foreach (var user in DbManager.Users.ToList())
             {
                 var userTrans=user.Transactions.Where(t => t.IsMtn == true).ToList().Where(t => t.timestamp.Date == DateTime.Today.Date);
                 if (userTrans!=null && userTrans.Count() > 0)
                     list.Add(user);
             }
             return list;
         }
    } 
  
   
}