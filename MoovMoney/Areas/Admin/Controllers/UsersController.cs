﻿using MoovMoney.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Data.Entity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Security;
using MoovMoney.Areas.Admin.ViewModels;
using Microsoft.AspNet.Identity;
using MoovMoney.Controllers;

namespace MoovMoney.Areas.Admin.Controllers
{
    [Authorize(Roles = "ADMIN, AUDITEUR")]
    public class UsersController : BaseController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        // GET: Admin/Caissiers
        public async Task<ActionResult> Index(string role)
        {
            List<ApplicationUser> users=new List<ApplicationUser>();
            if (string.IsNullOrEmpty(role))
                users = await UserManager.Users.ToListAsync();
            else
            {
                IdentityRole userRole = await RoleManager.FindByNameAsync(role);
                foreach (var user in UserManager.Users.ToList())
                {
                    if (await UserManager.IsInRoleAsync(user.Id, userRole.Name))
                    {
                        users.Add(user);
                    }
                }
            }
            return View(users.OrderBy(u => u.DateAjout).ToList());
        }


        public async Task<ActionResult> EnableDisableAccount(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);
            if( user!=null)
            {
                user.IsValidated = !user.IsValidated;
                await DbManager.SaveChangesAsync();
            }
            return RedirectToAction("Index");
                
        }

        

        public async Task<ActionResult> Add()
        {
            var agences=await DbManager.Agences.OrderBy(a=>a.Nom).ToListAsync();
            
            var usersVM = new AddUserViewModel();
            usersVM.Agences = agences.Select(a => new SelectListItem { Text = a.Nom, Value = a.Id.ToString() }).ToList();
            return View(usersVM);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddUserViewModel userVM)
        {
            if (ModelState.IsValid)
            {
                bool userExist = await UserAlreadyExistWithSameParams(userVM.Matricule, userVM.Email);
                string v = userExist.ToString();
                if (userExist)
                {

                    ModelState.AddModelError("", "Email ou matricule déja existant");
                }
                else
                {
                    string roleValue = Request.Form["role"];
                    ApplicationUser user = GetUserFrom(userVM);                   
                    var result = await UserManager.CreateAsync(user, user.TempPassword);
                    if (result.Succeeded)
                    {
                        await UserManager.AddToRoleAsync(user.Id,roleValue);
                        return RedirectToAction("Index", new {role=roleValue });
                    }
                    AddErrors(result);
                }
            }
            ViewBag.CantAddUser = true; 
            return View(userVM);
        }

        public async Task<ActionResult> Delete(string matricule)
        {
            var userToDelete = await UserManager.Users.FirstOrDefaultAsync(u => u.Matricule == matricule);
            if (userToDelete != null)
                await UserManager.DeleteAsync(userToDelete);
            return RedirectToAction("Index");
        }

        private ApplicationUser GetUserFrom(AddUserViewModel vm)
        {
            ApplicationUser user = new ApplicationUser()
            {
                Nom = vm.Nom,
                Prenoms = vm.Prenoms,
                UserName = vm.Email,
                AccountNumber=vm.AccountNumber,
                UserAgence=GetUserAgenceFrom(vm.Agence),
                Email = vm.Email,
                Matricule = vm.Matricule,
                PhoneNumber = vm.PhoneNumber,
                DateAjout=DateTime.Now,
                IsValidated=true,
                TempPassword = Membership.GeneratePassword(6,0)
            };
            return user;
        }

        private Agence GetUserAgenceFrom(string p)
        {
            int agenceId = int.Parse(p);
            return DbManager.Agences.FirstOrDefault(a => a.Id == agenceId);
        }

        private async Task<bool> UserAlreadyExistWithSameParams(string matricule, string email)
        {
            var foundUSer = await UserManager.Users.FirstOrDefaultAsync(u => u.Matricule == matricule);
            if(foundUSer==null)
                foundUSer = await UserManager.Users.FirstOrDefaultAsync(u => u.Email == email);
            return foundUSer != null;
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}