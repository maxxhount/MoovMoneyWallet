﻿using MoovMoney.Controllers;
using MoovMoney.Models;
using OfficeOpenXml;
using Syroot.Windows.IO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MoovMoney.Areas.Admin.Controllers
{
    [Authorize(Roles = "ADMIN, AUDITEUR")]
    public class TransactionController : BaseController
    {
        // GET: Admin/Transaction
        public async Task<ActionResult> Index()
        {
            var trans = await DbManager.Transactions.ToListAsync();
            return View(trans);
        }

        public ActionResult ExportToExcel()
        {
            try
            {
                var DirPath =KnownFolders.PublicDownloads.Path;
                String fileName =Path.Combine(DirPath,"transactions.xlsx");
                var trans = DbManager.Transactions.ToList();
                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets.Add("Transactions");
                    worksheet.Row(1).Height = 20;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Cells[1, 1].Value = "N°";
                    worksheet.Cells[1, 2].Value = "NOM";
                    worksheet.Cells[1, 3].Value = "PRENOMS";
                    worksheet.Cells[1, 4].Value = "NUMERO";
                    worksheet.Cells[1, 5].Value = "MONTANT";
                    worksheet.Cells[1, 6].Value = "DATE";
                    worksheet.Cells[1, 7].Value = "CAISSIER";
                    for (int i = 0; i < trans.Count; i++)
                    {
                        Transaction t = trans[i];
                        worksheet.Cells[i + 2, 1].Value = i + 1;
                        worksheet.Cells[i + 2, 2].Value = t.lastname;
                        worksheet.Cells[i + 2, 3].Value = t.firstname;
                        worksheet.Cells[i + 2, 4].Value = t.msisdn;
                        worksheet.Cells[i + 2, 5].Value = t.transamount;
                        worksheet.Cells[i + 2, 6].Value = t.timestamp.ToLongDateString();
                        worksheet.Cells[i + 2, 7].Value = t.User.UserName;
                        worksheet.Column(i + 1).AutoFit();
                    }

                    //Create excel file on physical disk    
                    FileInfo info = new FileInfo(fileName);
                    pck.SaveAs(info);
                }
                return File(fileName, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Transactions.xlsx");
            }
            catch (Exception)
            {               
                throw;
            }
            
        }
    }
}