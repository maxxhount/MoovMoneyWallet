﻿using MoovMoney.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MoovMoney.Areas.Admin.ViewModels;
using MoovMoney.Models;
using System.Data.Entity.Migrations;
using MoovMoney.Helpers;
using System.Text;

namespace MoovMoney.Areas.Admin.Controllers
{
    [Authorize(Roles = "ADMIN")]
    public class SettingsController : BaseController
    {
        // GET: Admin/Settings
        public async Task<ActionResult> Agences()
        {
            var agences = await DbManager.Agences.OrderBy(a=>a.Nom).ToListAsync();
            return View(new AgenceViewModel(){Agences=agences});
        }

        [HttpPost]
        public async Task<ActionResult> Agences(AgenceViewModel vm)
        {
            if(ModelState.IsValid)
            {
                var agence = new Agence() { Nom = vm.AgenceName };
                DbManager.Agences.AddOrUpdate(a => a.Nom, agence);
                await DbManager.SaveChangesAsync();
            }
            
            return RedirectToAction("Agences");
        }

        public async Task<ActionResult> Prefixes()
        {
            var moovprefixes = DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MOOV).Prefixes.ToList();
            var mtnprefixes = DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MTN).Prefixes.ToList();
            var vm = new PrefixeViewModel { MoovPrefixestring = GetStringPrefixes(moovprefixes),MtnPrefixestring=GetStringPrefixes(mtnprefixes) };
            return View(vm);
        }

        [HttpPost]
        public async Task<ActionResult> Prefixes(PrefixeViewModel vm)
        {
            if(ModelState.IsValid)
            {
                var moovprefixes = GetPrefixesFrom(vm.MoovPrefixestring);
                var mtnprefixes = GetPrefixesFrom(vm.MtnPrefixestring);
                DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MOOV).Prefixes.Clear();
                DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MTN).Prefixes.Clear();

                DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MOOV).Prefixes = moovprefixes;
                DbManager.Networks.FirstOrDefault(n => n.Name == Common.NetworkName.MTN).Prefixes = mtnprefixes;
                await DbManager.SaveChangesAsync();
            }
            
            return RedirectToAction("Prefixes");
        }

        public async Task<ActionResult> DeleteAgence(int id)
        {
            var agenceToDelete = DbManager.Agences.FirstOrDefault(a => a.Id == id);
            DbManager.Agences.Remove(agenceToDelete);
            await DbManager.SaveChangesAsync();
            return RedirectToAction("Agences");
        }

        private List<NetworkPrefix> GetPrefixesFrom(string prefString)
        {
            string[] prefs = prefString.Split(',');
            List<NetworkPrefix> preflist = new List<NetworkPrefix>();
            foreach (var ps in prefs)
                preflist.Add(new NetworkPrefix() { Prefix = ps });
            return preflist;
        }

        private string GetStringPrefixes(List<NetworkPrefix> prefixes)
        {
            StringBuilder str = new StringBuilder();
            foreach (var pref in prefixes.Select(p=>p.Prefix))
                str.Append(pref + " ,");
            return str.ToString();
        }
    }
}