﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoovMoney.Models;

namespace MoovMoney.Areas.Admin.ViewModels
{
    public class DashbordViewModel
    {
        public int CaissierCount { get; set; }
        public string MoovBalanceOfDay { get; set; }
        public string MtnBalanceOfDay { get; set; }
        public int OperationOfWeekCount { get; set; }
        public long TotalMerchent  { get; set; }
        public long TotalMerchentThisWeek { get; set; }

        public BarChartDataSet TransactionOfYearJson { get; set; }
        public List<Transaction> RecentTransactions { get; set; }
        public List<AgenceVM> AgenceTransactions { get; set; }
    }

    public class AgenceVM
    {
        public string AgenceName { get; set; }
        public string TotalAmount { get; set; }
    }
}