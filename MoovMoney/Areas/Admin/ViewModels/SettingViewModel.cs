﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoovMoney.Models;
using System.ComponentModel.DataAnnotations;

namespace MoovMoney.Areas.Admin.ViewModels
{
    public class SettingViewModel
    {
    }

    public class AgenceViewModel
    {
        public AgenceViewModel()
        {
            Agences = new List<Agence>();
        }

        [Display(Name="Nom de l'agence")]
        [Required(ErrorMessage = "Entrez le nom de l'agence")]
        public string AgenceName { get; set; }
        public List<Agence> Agences { get; set; }
    }

    public class PrefixeViewModel
    {
        [Display(Name = "Préfixes MOOV")]
        [Required]
        public string MoovPrefixestring { get; set; }
        [Display(Name = "Préfixes MOOV")]
        [Required]
        public string MtnPrefixestring { get; set; }
    }
}