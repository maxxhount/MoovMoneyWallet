﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoovMoney.Models;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MoovMoney.Areas.Admin.ViewModels
{
    public class UserControllerViewModel
    {
       
    }

    public class AddUserViewModel
    {
        public AddUserViewModel()
        {
            Agences = new List<SelectListItem>();
        }
        public string Nom { get; set; }
        public string Prenoms { get; set; }
        [Required(ErrorMessage = "Adresse email obligatoire")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Numero matricule obligatoire")]
        public string Matricule { get; set; }
        [Display(Name = "Numero de téléphone")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Veuillez choisir votre agence")]
        public string Agence { get; set; }
        public List<SelectListItem> Agences { get; set; }
        [Display(Name = "Numero de compte")]
        public string AccountNumber { get; set; }

    }
}