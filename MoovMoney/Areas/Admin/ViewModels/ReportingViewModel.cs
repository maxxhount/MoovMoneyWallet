﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MoovMoney.Models;

namespace MoovMoney.Areas.Admin.ViewModels
{
    public class ReportingViewModel
    {
        public ReportingViewModel()
        {
            Transactions = new List<Transaction>();
        }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string StartDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string EndDate { get; set; }
        public bool IsMoov { get; set; }
        public bool IsMtn { get; set; }
        public string PhoneNumber { get; set; }
        public string Agence { get; set; }
        public List<SelectListItem> Agences { get; set; }
        public List<Transaction> Transactions { get; set; }
        public bool IsPost { get; set; }
    }
}