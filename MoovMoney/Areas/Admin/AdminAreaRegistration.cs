﻿using System.Web.Mvc;

namespace MoovMoney.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Admin_users",
                "Admin/Users/{action}/{id}",
                new { controller = "Users", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}