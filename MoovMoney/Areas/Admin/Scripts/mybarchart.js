﻿


    var LoadChart=function(moovdata,mtndata)
    {
        if ($('#mybarChart1').length) {

            var ctx = document.getElementById("mybarChart1");
            var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"],
                    datasets: [{
                        label: 'MOOV',
                        backgroundColor: "#AFD003",
                        data: moovdata
                    },
                        {
                            label: 'MTN',
                            backgroundColor: "#FBCC0C",
                            data: mtndata
                        }
                    ]
                },

                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    responsive: true,
                    maintainAspectRatio: false
                }
            });
        }
    }
