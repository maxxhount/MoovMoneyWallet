﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace MoovMoney.Helpers
{
    public class PriceFormater
    {
        private static NumberFormatInfo info=new NumberFormatInfo { NumberGroupSeparator = " ", NumberDecimalDigits = 0 };
        public static string Format(long price)
        {
            return price.ToString("N", info);
        }
    }
}