﻿using MoovMoney.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Helpers
{
    public class RequestCountHelper
    {
        private static RequestCountHelper _instance;

        public static RequestCountHelper Instance
        {
            get 
            {
                return _instance??new RequestCountHelper(); 
            }           
        }


        private int _count;

        public int Counter
        {
            get 
            {
                _count = GetRequestCounterAsync();
                return _count; 
            }
            set 
            {
                _count = value;
                SaveRequestCounter(_count);
            }
        }
        
        private int GetRequestCounterAsync()
        {
            int value = DbManager.RequestCounter.FirstOrDefault().Value;
            int count=value>1024?0:value;
            return count;
        }

        private void SaveRequestCounter(int value)
        {
            var rc = DbManager.RequestCounter.FirstOrDefault();
            rc.Value = value;
            DbManager.SaveChanges();
        }




         private ApplicationDbContext _dbManager;
         public ApplicationDbContext DbManager
         {
             get
             {
                 return _dbManager ?? ApplicationDbContext.Create();
             }
             private set
             {
                 _dbManager = value;
             }
         }
        
    }
}