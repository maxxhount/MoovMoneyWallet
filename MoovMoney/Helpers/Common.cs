﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoovMoney.Helpers
{
    public class Common
    {
        public class TotalCalculator
        {
            public static string TotalTransactionsOfDay { get; set; }
        }

        public static class RoleConstant
        {
            public static string ADMIN { get { return "ADMIN"; } }
            public static string CAISSIER { get { return "CAISSIER"; } }
            public static string AUDITEUR { get { return "AUDITEUR"; } }
        }

        public static class NetworkName
        {
            public static string MOOV { get { return "MOOVBENIN"; } }
            public static string MTN { get { return "MTNBENIN"; } }
        }
    }
}