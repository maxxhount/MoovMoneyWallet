﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace MoovMoney.Helpers
{
    public class XmlDeserializer
    {



        public static T XmlDeserialize<T>(string sourceValue) where T : class
        {
            // If source is empty, throw Exception
            if (string.IsNullOrWhiteSpace(sourceValue))
                throw new NullReferenceException("sourceValue is required");

            // Define encoding
            var encoding = Encoding.ASCII;

            // Declare the resultant variable
            T targetValue;

            // Declare Xml Serializer with target value Type (serialized type)
            var xmlSerializer = new XmlSerializer(typeof(T));

            // Get the source value to bytes with required Encoding
            byte[] sourceBytes = encoding.GetBytes(sourceValue);

            // Using MemoryStream for In-Process conversion
            using (var memoryStream = new MemoryStream(sourceBytes))
            {
                // Read stream into XML-based reader
                using (var xmlTextReader = new XmlTextReader(memoryStream))
                {
                    // Perform Deserialization from the stream and Convert to target type
                    targetValue = xmlSerializer.Deserialize(xmlTextReader) as T;
                }
            }

            // Return the resultant value;
            return targetValue;
        }
    }
}