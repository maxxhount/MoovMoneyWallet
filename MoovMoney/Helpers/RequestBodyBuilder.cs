﻿using MoovMoney.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Owin;


namespace MoovMoney.Helpers
{
    public class RequestBodyBuilder
    {
        private string token;
        private string bankToWalletXmlString = Resources.Constant.BankToWalletXml;
        private string statusXmlString = Resources.Constant.StatusXml;
        //private string parameters = String.Format("10:{0}:{1}", Resources.Constant.Username, Resources.Constant.Password);
        public RequestBodyBuilder(int count)
        {
            string parameters = String.Format("{0}:{1}:{2}",count, Resources.Constant.Username, Resources.Constant.Password);
            token = EASCrytoHelper.Instance.EncryptText(parameters);
        }

        private void IncrementRequestCounter(int count)
        {
            RequestCountHelper.Instance.Counter = count+1;
        }

       
        
        public string GetBankToWalletRequestBody(Transaction t)
        {
            return String.Format(bankToWalletXmlString
                ,token
                , t.banktransactionreferenceid
                , t.msisdn,
                t.firstname,
                t.lastname,
                t.secondname,
                t.timestamp,
                t.accountnumber,
                t.transamount,
                t.bankbalancebefore,
                t.bankbalanceafter
                );
        }

        public string GetStatusRequestBody(string msisdn)
        {
            return string.Format(statusXmlString,token, msisdn);
        }

        
    }
}