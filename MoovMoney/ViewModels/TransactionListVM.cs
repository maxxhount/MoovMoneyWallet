﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoovMoney.Models;

namespace MoovMoney.ViewModels
{
    public class TransactionListVM
    {
        public string PhoneNumber { get; set; }
        public string DateString { get; set; }
        public IEnumerable<Transaction> Transactions { get; set; }
    }
}