﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoovMoney.ViewModels
{
    public class AddOperationVM
    {
        [Required(ErrorMessage="Entrez le nom du client")]
        public string Nom { get; set; }

        [Required(ErrorMessage = "Entrez le prénom du client")]
        public string Prenoms { get; set; }

       [Required(ErrorMessage = "Entrez le numero de téléphone du client")]
       [StringLength(8,MinimumLength=8, ErrorMessage = "Le numero doit contenir 8 chiffres")]
       [DataType(DataType.PhoneNumber, ErrorMessage = "Le numero est incorrect")]
       [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Le numero est incorrect")]
        public string Numero { get; set; }

       [Required(ErrorMessage = "Confirmez le numero de téléphone du client")]
       [Compare("Numero", ErrorMessage = "Le numero ne correspond pas")]
       [DataType(DataType.PhoneNumber, ErrorMessage = "Le numero est incorrect")]
       public string NumeroConfirm { get; set; }

        [DataType(DataType.Currency,ErrorMessage="Le montant est incorrect")]
        [Required(ErrorMessage = "Entrez le montant")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Le montant n'est pas valide")]
        public string Montant { get; set; }

    }
}